import colors from "vuetify/es5/util/colors";
export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  ssr: false,
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "static",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    meta: [
      { charset: "utf-8" },
      {
        name: "viewport",
        content:
          "width=device-width, initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=yes"
      },
      {
        hid: "og:type",
        property: "og:type",
        content: "website"
      },
      {
        hid: "og:title",
        property: "og:title",
        content: "Compre, ajude e concorra a prêmios incríveis"
      },
      {
        hid: "og:image",
        property: "og:image",
        content: "/og_imagem.png"
      },
      {
        hid: "og:image:type",
        property: "og:image:type",
        content: "image/png"
      },
      {
        hid: "og:description",
        property: "og:description",
        content:
          "Você pode concorrer a prêmios incríveis, fazer doações e ajudar quem mais precisa."
      }
    ],

    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }]
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    "@/plugins/vuetify",
    "@/plugins/filters",
    "@/plugins/axios",
    "@/plugins/resources",
    "@/plugins/firebase",
    { src: "@/plugins/localStorage.js", ssr: false },
    "@/plugins/google",
    "@/plugins/facebook"
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    "@nuxt/typescript-build",
    "@nuxtjs/vuetify",
    "@nuxtjs/fontawesome"
  ],
  fontawasome: {
    component: "fa",
    icons: {
      solid: true,
      brands: true
    }
  },
  /*
   ** Nuxt.js modules
   */
  modules: ["vue-sweetalert2/nuxt", "vue-scrollto/nuxt", "@nuxtjs/sentry"],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ["~assets/estilos/variables.scss"],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {}
};
