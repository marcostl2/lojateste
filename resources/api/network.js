import axios from "axios";

const apiUrl = process.env.api_url;
const apiPdvUrl = process.env.api_pdv_url;
const config = {
  timeout: 60000,
  exibePracaId: true
};

export default class Network {
  async login(data) {
    const response = await axios.post(`${apiUrl}/login`, data, config);
    return response;
  }

  async cadastraCliente(data) {
    return await axios.post(`${apiUrl}/cliente`, data, config);
  }

  async canaisRecuperarSenha() {
    return await axios.get(`${apiUrl}/v2/recuperar`, config);
  }

  async enviarCodigoRecuperarSenha(data) {
    return await axios.post(`${apiUrl}/recuperar1`, data, config);
  }

  async enviarNovaSenhaRecuperarSenha(data) {
    return await axios.post(`${apiUrl}/recuperar2`, data, config);
  }

  async enderecoPorCEP(cep) {
    const response = await axios.get(
      `https://viacep.com.br/ws/${cep}/json`,
      config
    );
    return response;
  }

  async carregaEtapas() {
    return await axios.get(`${apiUrl}/v2/etapas`, config);
  }

  async obterTitulos(idEtapa, tipo, numeros) {
    let queryParams = "";
    queryParams += idEtapa ? `&etapa_id=${idEtapa}` : "";
    queryParams += tipo ? `&tipo=${tipo}` : "";
    queryParams += numeros ? `&numeros=${numeros}` : "";
    const response = await axios.get(
      `${apiUrl}/certificados?${queryParams}`,
      config
    );
    return response;
  }

  async carregaMeiosDePagamentos(etapa_id) {
    return await axios.get(`${apiUrl}/carteira?etapa_id=${etapa_id}`, config);
  }

  async carregaMeiosDePagamentosCompraSemLogin(etapa_id) {
    return await axios.get(
      `${apiUrl}/v2/formasPagamentos?etapa_id=${etapa_id}`,
      config
    );
  }

  async comprarTitulosDebito(data) {
    return await axios.post(`${apiUrl}/sales/checkout`, data, config);
  }

  async comprarTitulosDebitoSemLogin(data) {
    return await axios.post(
      `${apiUrl}/v2/vendasRapidas/checkouts`,
      data,
      config
    );
  }

  async comprarTitulosCredito(data) {
    return await axios.post(`${apiUrl}/sales`, data, config);
  }

  async comprarTitulosCreditoSemLogin(data) {
    return await axios.post(`${apiUrl}/v2/vendasRapidas`, data, config);
  }

  async informacaoDaCompra(idCompra) {
    return await axios.get(`${apiUrl}/sales/checkout/${idCompra}/info`, config);
  }

  async deletarCartaoDeCredito(token) {
    return await axios.delete(`${apiUrl}/carteira/${token}`, config);
  }

  async historicoDeTitulos() {
    return await axios.get(`${apiUrl}/certificadosHistorico`, config);
  }

  async atualizaDadosCliente(data) {
    return await axios.put(`${apiUrl}/cliente`, data, config);
  }

  async alteraSenha(data) {
    return await axios.put(`${apiUrl}/cliente/password`, data, config);
  }

  async enviarFeedback(data) {
    return await axios.post(`${apiUrl}/feedback`, data, config);
  }

  async dadosContaCorrente() {
    return await axios.get(`${apiUrl}/api/v2/contaCorrente`, config);
  }

  async dadosIndiqueEGanhe() {
    return await axios.get(`${apiUrl}/v2/mgm`, config);
  }

  async gerarBoletodata(data) {
    return await axios.post(`${apiUrl}/v2/contaCorrente`, data, config);
  }

  async consultaCadastro(data) {
    return await axios.post(`${apiUrl}/v2/clientes/verificacoes`, data, config);
  }
  async carregaFAQ() {
    return await axios.get(`${apiUrl}/faq`, config);
  }

  async informacaoPraca(codigoPraca) {
    return await axios.get(`${apiUrl}/v2/pracas?codigo_praca=${codigoPraca}`, {
      ...config,
      exibePracaId: false
    });
  }

  async validacoes(data) {
    return await axios.post(`${apiUrl}/v2/validacoes`, data, config);
  }

  async pdvConsultaCadastro(data) {
    return await axios.post(
      `${apiPdvUrl}/pdv/clientesVendedores`,
      data,
      config
    );
  }

  async raspadinhasTodasEtapas() {
    return await axios.get(`${apiUrl}/raspadinhas`, config);
  }
  async raspadinhasPorEtapas(etapa_id) {
    return await axios.get(`${apiUrl}/raspadinhas/${etapa_id}`, config);
  }
  async raspadinhasRaspou(data, venda_id) {
    return await axios.post(`${apiUrl}/raspadinhas/${venda_id}`, data, {
      ...config,
      exibePracaId: false
    });
  }

  async listaVendedores(filtro) {
    return await axios.get(`${apiUrl}/api/v2/vendedores`, {
      ...config,
      params: { filtro }
    });
  }
}
