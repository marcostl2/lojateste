import axios from "axios";

export default class Firebase {
  constructor(store) {
    this.store = store;
  }

  async getAparencia(code) {
    let res = await axios.get(
      `${process.env.firebase_config.databaseURL}/${code}/config/aparencia.json`,
      { timeout: 60000 }
    );
    return res;
  }

  async getLojaOnline(code) {
    let res = await axios.get(
      `${process.env.firebase_config.databaseURL}/${code}/loja_online.json`,
      { timeout: 60000 }
    );
    return res;
  }
}
