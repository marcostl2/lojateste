# arrays

dirs=("indique" "meuTime" "produtosAdquiridos")
metas=("indique-ganhe" "meu-time" "produtos-adquiridos")

# part of the old head to use in new
# d -> delimiter / s -> start / e -> end

ds_head='<meta data-n-head="1" data-hid="og:type" property="og:type" content="website">'
de_head='<link data-n-head="1" rel="icon" type="image/x-icon" href="/favicon.ico">'

# vars

working_dir='dist/wp'
new_meta_dir='meta_property'
index='index.html'

make_new_meta() {

    old_meta=$(sed -n "$((3+1))p" $1)

    sed -n "1,${3%:*}p" $1 > aux.html
    echo ${old_meta%$ds_head*} >> aux.html
    cat $2 >> aux.html
    echo ${old_meta#*$de_head} >> aux.html
    sed -n "${4%:*},${5%:*}p" $1 >> aux.html

    cat aux.html > $1
    rm -f aux.html
}

for i in ${!dirs[@]}; do

    to_change="$working_dir/${dirs[i]}/$index" # $1
    new_meta="$new_meta_dir/${metas[i]}" # $2
    n_ln_head_s=$(grep -n "<head>" $to_change) # $3
    n_ln_head_e=$(grep -n "</head>" $to_change) # $4
    n_ln_html_e=$(grep -n "</html>" $to_change) # $5
    
    make_new_meta "$to_change" "$new_meta" "$n_ln_head_s" "$n_ln_head_e" "$n_ln_html_e"

done