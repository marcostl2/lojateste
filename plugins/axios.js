import axios from "axios";

export default function(ctx) {
  axios.interceptors.request.use(
    config => {
      if (config.url.startsWith(process.env.api_url)) {
        const pracaConfig = ctx.store.getters["praca/config"];
        if (pracaConfig && pracaConfig.idPraca && config.exibePracaId) {
          config.params = { ...config.params, praca_id: pracaConfig.idPraca };
        }
        if (ctx.store.state.usuario.logado) {
          config.headers[
            "Authorization"
          ] = `bearer ${ctx.store.state.usuario.token}`;
        }
      }
      // webpages
      if (
        ["wp-aceite", "wp-meuTime", "wp-produtosAdquiridos"].includes(
          ctx.route.name
        )
      ) {
        config.params = {
          ...config.params,
          praca_id: ctx.route.query.p
        };
      }
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  );

  axios.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      if (error.response && error.response.status == 401) {
        ctx.store.commit("usuario/logout");
        ctx.redirect(`/entrar`);
      }
      return Promise.reject(error);
    }
  );
}
