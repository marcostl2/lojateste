import createPersistedState from "vuex-persistedstate";

export default ({ store }) => {
  createPersistedState({
    key: "XGb5fbPq4Rh8aRzFo",
    paths: [
      "compra.transacaoPendente",
      "compra.checkoutPendente",
      "compra.tipoFluxoCompra",
      "usuario.logado",
      "usuario.dados",
      "usuario.token",
      "usuario.lembrarLogin",
      "usuario.loginSalvo",
      "usuario.origemTrafego",
      "usuario.capturaLeadPersistente",
      "praca.nomePasta",
      "praca.tipoUnica",
      "praca.idEtapaAtual",
      "layout.avisoCookie"
    ]
  })(store);
};
