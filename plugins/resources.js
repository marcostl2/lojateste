import Network from "~/resources/api/network";
import Firebase from "~/resources/api/firebase";

export default (context, inject) => {
  const network = new Network(context.store);
  inject("networkApi", network);

  const firebase = new Firebase(context.store);
  inject("firebaseApi", firebase);
};
