

export default (context, inject) => {
  inject("googleAnalyticsStart", () => googleAnalyticsStart(context));
  inject("googleAdsStart", () => googleAdsStart(context));
  inject("gtagEvent", (event, params) => gtagEvent(event, params, context));
  inject("gaPageView", params => gaPageView(params, context));
};

window.dataLayer = window.dataLayer || [];
function gtag() {
  dataLayer.push(arguments);
}

async function loadGoogleAnalytics(id) {
  var ga = document.createElement("script");
  ga.type = "text/javascript";
  ga.async = true;
  ga.src = "https://www.googletagmanager.com/gtag/js?id=" + id;

  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(ga, s);
}

function googleAnalyticsStart(context) {
  const googleAnalytics = [].concat(
    context.store.getters["praca/config"].monitoramento.googleAnalytics
  );
  googleAnalytics.forEach((ga, i) => {
    if (i == 0) {
      loadGoogleAnalytics(ga.tag);
      gtag("js", new Date());
    }
    gtag("config", ga.tag, { send_page_view: false });
  });
}

function googleAdsStart(context) {
  const googleAds = [].concat(
    context.store.getters["praca/config"].monitoramento.googleAds
  );
  googleAds.forEach((gads, i) => {
    gtag("config", gads.conversion_id);
  });
}

function gaPageView(params, context) {
  const googleAnalytics = [].concat(
    context.store.getters["praca/config"].monitoramento.googleAnalytics
  );
  googleAnalytics.forEach(ga => {
    gtag("config", ga.tag, { send_page_view: false });
    gtag("event", "page_view", {
      ...params,
      send_to: ga.tag
    });
  });
}

function gtagEvent(event, params, context) {
  const googleAnalytics = [].concat(
    context.store.getters["praca/config"].monitoramento.googleAnalytics
  );
  const googleAds = [].concat(
    context.store.getters["praca/config"].monitoramento.googleAds
  );
  if (googleAnalytics.length > 0) {
    switch (event.toUpperCase()) {
      case "GOOGLE_LOGIN":
      case "INTERNAL_LOGIN":
        gtag("event", "login");
        break;

      case "SIGN_UP":
        gtag("event", "sign_up");
        break;

      case "ADD_TO_CART":
        gtag("event", "add_to_cart", { items: formatItems([params]) });
        break;

      case "REMOVE_FROM_CART":
        gtag("event", "remove_from_cart", {
          items: formatItems([params.card], params.position)
        });
        break;

      case "BEGIN_CHECKOUT":
        gtag("event", "begin_checkout", { items: formatItems(params) });
        break;

      case "CHECKOUT_PROGRESS":
        gtag("event", "checkout_progress", { items: formatItems(params) });
        break;

      case "PURCHASE":
        gtag("event", "purchase", {
          transaction_id: params.orderId,
          affiliation: params.affiliation,
          value: params.cards
            .reduce((total, item) => total + item.valor, 0)
            .toFixed(2),
          currency: "BRL",
          promotion_id: "Store",
          promotion_name: "Store",
          items: formatItems(params.cards),
          payment_type: params.checkoutOption
        });

        for (let i = 0; i < googleAds.length; i++) {
          gtag("event", "conversion", {
            send_to: `${googleAds[i].conversion_id}/${googleAds[i].label_id_purchase}`,
            transaction_id: params.orderId,
            value: params.cards.reduce((total, item) => total + item.valor, 0),
            currency: "BRL"
          });
        }
        break;

      case "CANCEL_CHECKOUT":
        gtag("event", "cancel_checkout", {
          event_category: "ecommerce",
          value: params.reduce((total, item) => total + item.valor, 0)
        });
        break;

      case "VIEW_ITEM":
        gtag("event", "view_item", { items: formatItems([params]) });
        break;

      case "APP_SEM_LOGIN":
        gtag("event", "app-sem-login");

        break;
      default:
        break;
    }
  }

  function formatItems(items, position) {
    return items.map((p, index) => {
      let sku;
      let name;

      if (p.sequencia3) {
        sku = "TituloTriplo";
        name = "Título Triplo";
      } else if (p.sequencia2) {
        sku = "TituloDuplo";
        name = "Título Duplo";
      } else if (p.sequencia1) {
        sku = "TituloSimples";
        name = "Título Simples";
      } else {
        sku = "Desconhecido";
        name = "Desconhecido";
      }

      return {
        item_id: sku,
        item_name: name,
        item_category: "Títulos",
        index: position ? position : index + 1,
        quantity: 1,
        currency: "BRL",
        price: p.valor.toFixed(2)
      };
    });
  }
}
