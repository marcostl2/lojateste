import Vue from "vue";

Vue.filter("formatDate", function(value, format = "DD/MM/YYYY") {
  if (value) {
    return moment.parseZone(value).format(format);
  }
});

Vue.filter("formatPhone", function(value) {
  if (value && value.length == 10) {
    return value
      .replace(/[^0-9]/g, "")
      .replace(/(\d{2})(\d{4})(\d{4})/, "($1) $2-$3");
  } else if (value && value.length == 11) {
    return value
      .replace(/[^0-9]/g, "")
      .replace(/(\d{2})(\d{5})(\d{4})/, "($1) $2-$3");
  }
  return value;
});

Vue.filter("formatCpf", function(value) {
  if (!value) return;

  if (value.length == 11) {
    return value
      .replace(/[^0-9]/g, "")
      .replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
  }
  return value;
});

Vue.filter("formatTime", function(value) {
  return value.replace(/[^0-9]/g, "").replace(/(\d{2})(\d{2})/, "$1:$2");
});

Vue.filter("formatCurrency", function(value) {
  if (typeof value !== "number") {
    return value;
  }
  let formatter = new Intl.NumberFormat("pt-br", {
    style: "currency",
    currency: "BRL"
  });
  return formatter.format(value);
});

Vue.filter("somenteNumeros", function(value) {
  if (!value) return;

  return value.replace(/[^0-9]/g, "");
});
