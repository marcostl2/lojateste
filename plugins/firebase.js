var firebase = require("firebase/app");
require("firebase/auth");
require("firebase/database");


export default (context, inject) => {
  firebase.initializeApp(process.env.firebase_config);
};

export const database =  firebase.database;
export const auth = firebase.auth;