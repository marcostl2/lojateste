export default (context, inject) => {
  inject("facebookPixelStart", () => facebookPixelStart(context));
  inject("fpPageView", () => fpPageView(context));
  inject("fpEvent", (event, params) => fpEvent(event, params, context));
};

async function loadFacebookPixel(ids) {
  !(function(f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function() {
      n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments);
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = "2.0";
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s);
  })(
    window,
    document,
    "script",
    "https://connect.facebook.net/en_US/fbevents.js"
  );

  for (let i = 0; i < ids.length; i++) {
    let id = ids[i];
    fbq("init", id);
  }
}

async function facebookPixelStart(context) {
  const facebookPixel = [].concat(
    context.store.getters["praca/config"].monitoramento.facebookPixel
  );

  if (facebookPixel.length > 0) {
    await loadFacebookPixel(facebookPixel);
  }
}

async function fpPageView(context) {
  const facebookPixel = [].concat(
    context.store.getters["praca/config"].monitoramento.facebookPixel
  );
  if (facebookPixel.length > 0) {
    fbq("track", "PageView");
  }
}

function fpEvent(event, params, context) {
  const facebookPixel = [].concat(
    context.store.getters["praca/config"].monitoramento.facebookPixel
  );
  if (facebookPixel.length > 0) {
    switch (event.toUpperCase()) {
      case "GOOGLE_LOGIN":
      case "INTERNAL_LOGIN":
        break;

      case "SIGN_UP":
        fbq("track", "CompleteRegistration");
        break;

      case "ADD_TO_CART":
        fbq("track", "AddToCart", formatItems([params]));
        break;

      case "REMOVE_FROM_CART":
        break;

      case "BEGIN_CHECKOUT":
        fbq("track", "InitiateCheckout", formatItems(params));
        break;

      case "CHECKOUT_PROGRESS":
        fbq("track", "AddPaymentInfo", formatItems(params));
        break;

      case "PURCHASE":
        fbq("track", "Purchase", formatItems(params.cards));
        break;

      case "CANCEL_CHECKOUT":
        break;

      case "VIEW_ITEM":
        break;

      default:
        break;
    }
  }

  function formatItems(items) {
    const obj = {
      content_ids: [],
      content_name: "Titulos",
      content_category: "Titulos",
      content_type: "product",
      contents: [],
      num_items: 0,
      currency: "BRL",
      value: 0
    };

    items.forEach(item => {
      let sku;
      let name;
      if (item.sequencia3) {
        sku = "TituloTriplo";
        name = "Titulo Triplo";
      } else if (item.sequencia2) {
        sku = "TituloDuplo";
        name = "Titulo Duplo";
      } else if (item.sequencia1) {
        sku = "TituloSimples";
        name = "Titulo Simples";
      } else {
        sku = "Desconhecido";
        name = "Desconhecido";
      }
      obj.content_ids.push(sku);
      obj.contents.push({
        id: sku,
        name: name,
        quantity: 1,
        currency: "BRL",
        value: item.valor
      });
      obj.value += item.valor;
      obj.num_items++;
    });

    return obj;
  }
}
