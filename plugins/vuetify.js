import "@fortawesome/fontawesome-free/css/all.css";
import Vue from "vue";
import VueTheMask from "vue-the-mask";
import Vuetify from "vuetify/lib";

Vue.use(VueTheMask);

Vue.use(Vuetify);

const opts = {};

export default new Vuetify(opts);
