export const state = () => ({
  nomePasta: null,
  tipoUnica: false,
  aparencia: null,
  config: null,
  etapas: null,
  idEtapaAtual: null,
  titulos: null,
  nomeProdutoSingular: null,
  nomeProdutoPlural: null
});

export const getters = {
  carregou(state) {
    return (
      !!state.nomePasta && !!state.aparencia && !!state.config && !!state.etapas
    );
  },
  aparencia(state) {
    return state.aparencia;
  },
  carregada(state) {
    return state.config != null;
  },
  nomePasta(state) {
    return state.nomePasta;
  },
  config(state) {
    return state.config;
  },
  tipoUnica(state) {
    return state.tipoUnica;
  },
  etapaAtual(state) {
    let etapaPorId = [];
    if (state.idEtapaAtual) {
      etapaPorId = state.etapas.etapas.filter(e => e.id === state.idEtapaAtual);
    }
    return etapaPorId.length > 0
      ? etapaPorId[0]
      : state.etapas && state.etapas.etapas.length > 0
      ? state.etapas.etapas[0]
      : null;
  },
  etapas(state) {
    return state.etapas;
  },
  idEtapaAtual(state) {
    return state.idEtapaAtual;
  },
  titulos(state) {
    return state.titulos;
  },
  nomeProdutoSingular(state) {
    return state.config.textos.nome_produto_singular
    ? state.config.textos.nome_produto_singular
    : "título";
  },
  nomeProdutoPlural(state) {
    return state.config.textos.nome_produto_plural
    ? state.config.textos.nome_produto_plural
    : "títulos";
  },


};


export const mutations = {
  salvaAparencia(state, data) {
    state.aparencia = data;
  },
  salvaNomePasta(state, data) {
    state.nomePasta = data;
  },
  salvaTipoUnica(state, data) {
    state.tipoUnica = data;
  },
  salvaConfig(state, data) {
    state.config = montaConfig(data);
    state.selecionada = true;
  },
  salvaEtapa(state, data) {
    state.etapas = data;
  },
  salvaTitulos(state, data) {
    state.titulos = data;
  },
  salvaTextos(state, data) {
    state.textos = data;
   
  },
  adicionaTitulos(state, data) {
    state.titulos.list = state.titulos.list.concat(data.list);
  },
  limpaTitulos(state) {
    if (state.titulos && state.titulos.list) {
      state.titulos.list = [];
    } else {
      state.titulos = null;
    }
  },
  salvaIdEtapaAtual(state, data) {
    state.idEtapaAtual = data;
  }
};

function montaConfig(configPraca) {
  const dataPracaAPI = configPraca.dataPracaAPI;
  const dataLojaOnline = configPraca.dataLojaOnline;
  return {
    nome: dataPracaAPI.nome ? dataPracaAPI.nome : "",
    idPraca: dataPracaAPI.id ? dataPracaAPI.id : null,
    codigoPraca: dataPracaAPI.codigo_praca ? dataPracaAPI.codigo_praca : null,
    qtdBolasSorteio:
      dataPracaAPI.configuracoes &&
      dataPracaAPI.configuracoes.qtde_bolas_sorteio
        ? dataPracaAPI.configuracoes.qtde_bolas_sorteio
        : null,
    configuracoes: dataPracaAPI.configuracoes || null,
    dados: {
      numerosEntidade:
        dataLojaOnline.dados && dataLojaOnline.dados.numeros_entidade
          ? dataLojaOnline.dados.numeros_entidade
          : []
    },
    estilos: {
      coresBase: {
        cor1: dataLojaOnline.estilos.cores_base.cor1,
        cor2: dataLojaOnline.estilos.cores_base.cor2,
        cor3: dataLojaOnline.estilos.cores_base.cor3,
        cor4: dataLojaOnline.estilos.cores_base.cor4,
        cor5: dataLojaOnline.estilos.cores_base.cor5
      },
      coresEspeciais: {
        gradiente1: {
          fim: dataLojaOnline.estilos.cores_especiais.gradiente1.fim,
          pFim: dataLojaOnline.estilos.cores_especiais.gradiente1.p_fim
            ? dataLojaOnline.estilos.cores_especiais.gradiente1.p_fim
            : "100",
          inicio: dataLojaOnline.estilos.cores_especiais.gradiente1.inicio,
          pInicio: dataLojaOnline.estilos.cores_especiais.gradiente1.p_inicio
            ? dataLojaOnline.estilos.cores_especiais.gradiente1.p_inicio
            : "0"
        },
        gradiente2: {
          fim: dataLojaOnline.estilos.cores_especiais.gradiente2.fim,
          pFim: dataLojaOnline.estilos.cores_especiais.gradiente2.p_fim
            ? dataLojaOnline.estilos.cores_especiais.gradiente2.p_fim
            : "100",
          inicio: dataLojaOnline.estilos.cores_especiais.gradiente2.inicio,
          pInicio: dataLojaOnline.estilos.cores_especiais.gradiente2.p_inicio
            ? dataLojaOnline.estilos.cores_especiais.gradiente2.p_inicio
            : "0"
        }
      },
      coresEspecificas: {
        barraEndereco: dataLojaOnline.estilos.cores_especificas.barra_endereco,
        titulo1: dataLojaOnline.estilos.cores_especificas.titulo1,
        titulo2: dataLojaOnline.estilos.cores_especificas.titulo2,
        titulo3: dataLojaOnline.estilos.cores_especificas.titulo3
      }
    },
    imagens: {
      backgroundNumerosEntidade:
        dataLojaOnline.imagens &&
        dataLojaOnline.imagens.background_numeros_entidade
          ? dataLojaOnline.imagens.background_numeros_entidade
          : null,
      logoEntidadeQuadro:
        dataLojaOnline.imagens && dataLojaOnline.imagens.logo_entidade_quadro
          ? dataLojaOnline.imagens.logo_entidade_quadro
          : null,
      logoEntidadeTopo:
        dataLojaOnline.imagens && dataLojaOnline.imagens.logo_entidade_topo
          ? dataLojaOnline.imagens.logo_entidade_topo
          : null,
      logoProjetoRodape:
        dataLojaOnline.imagens && dataLojaOnline.imagens.logo_projeto_rodape
          ? dataLojaOnline.imagens.logo_projeto_rodape
          : null,
      logoEntidadeRodape:
        dataLojaOnline.imagens && dataLojaOnline.imagens.logo_entidade_rodape
          ? dataLojaOnline.imagens.logo_entidade_rodape
          : null,
      logoDistribuidoraRodape:
        dataLojaOnline.imagens &&
        dataLojaOnline.imagens.logo_distribuidora_rodape
          ? dataLojaOnline.imagens.logo_distribuidora_rodape
          : null,
      logoProjeto:
        dataLojaOnline.imagens && dataLojaOnline.imagens.logo_projeto
          ? dataLojaOnline.imagens.logo_projeto
          : null,
      premios:
        dataLojaOnline.imagens && dataLojaOnline.imagens.premios
          ? dataLojaOnline.imagens.premios
          : null,
      comoParticiparImgSlide1:
        dataLojaOnline.imagens &&
        dataLojaOnline.imagens.como_participar_img_slide1
          ? dataLojaOnline.imagens.como_participar_img_slide1
          : null,
      comoParticiparImgSlide2:
        dataLojaOnline.imagens &&
        dataLojaOnline.imagens.como_participar_img_slide2
          ? dataLojaOnline.imagens.como_participar_img_slide2
          : null,
      pwa192x192:
        dataLojaOnline.imagens && dataLojaOnline.imagens.pwa_192x192
          ? dataLojaOnline.imagens.pwa_192x192
          : null,
      pwa512x512:
        dataLojaOnline.imagens && dataLojaOnline.imagens.pwa_512x512
          ? dataLojaOnline.imagens.pwa_512x512
          : null
    },
    monitoramento: {
      facebookPixel:
        dataLojaOnline.monitoramento &&
        dataLojaOnline.monitoramento.facebook_pixel
          ? dataLojaOnline.monitoramento.facebook_pixel
          : [],
      googleAds:
        dataLojaOnline.monitoramento && dataLojaOnline.monitoramento.google_ads
          ? dataLojaOnline.monitoramento.google_ads
          : [],
      googleAnalytics:
        dataLojaOnline.monitoramento &&
        dataLojaOnline.monitoramento.google_analytics
          ? dataLojaOnline.monitoramento.google_analytics
          : []
    },
    textos: {
      nome_produto_plural: dataLojaOnline.textos.nome_produto_plural,
      nome_produto_singular: dataLojaOnline.textos.nome_produto_singular,
      textoAjuda: dataLojaOnline.textos.texto_ajuda,
      textoEntidade: dataLojaOnline.textos.texto_entidade,
      tituloAjuda: dataLojaOnline.textos.titulo_ajuda,
      tituloEntidade: dataLojaOnline.textos.titulo_entidade,
      tituloPremio: dataLojaOnline.textos.titulo_premio,
      textoPremio: dataLojaOnline.textos.texto_premio,
      textoBemVindo: dataLojaOnline.textos.texto_bem_vindo
    },
    ganhadores: dataLojaOnline.ganhadores || []
  };
}

