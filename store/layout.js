export const state = () => ({
  menuAberto: false,
  dialogComoFuncionaAberto: false,
  dialogMultiEtapasAberto: false,
  dialogCapturarLeadAberto: false,
  dialogArquivoHTML: {
    aberto: false,
    titulo: "",
    link: ""
  },
  dialogIframe: {
    aberto: false,
    link: ""
  },
  dialogCondicoesGeraisAberto: false,
  avisoCookie: {
    idPracaAceito: null
  }
});

export const getters = {
  menuAberto(state) {
    return state.menuAberto;
  },
  dialogComoFuncionaAberto(state) {
    return state.dialogComoFuncionaAberto;
  },
  dialogMultiEtapasAberto(state) {
    return state.dialogMultiEtapasAberto;
  },
  dialogCapturarLeadAberto(state) {
    return state.dialogCapturarLeadAberto;
  },
  dialogArquivoHTML(state) {
    return state.dialogArquivoHTML;
  },
  dialogIframe(state) {
    return state.dialogIframe;
  },
  avisoCookie(state) {
    return state.avisoCookie;
  },
  dialogCondicoesGeraisAberto(state) {
    return state.dialogCondicoesGeraisAberto;
  }
};
export const mutations = {
  alteraAvisoCookie(state, data) {
    return (state.avisoCookie = data);
  },
  alteraMenu(state, data) {
    state.menuAberto = data != undefined ? data : !state.menuAberto;
  },
  alteraDialogComoFuncionaAberto(state, data) {
    state.dialogComoFuncionaAberto =
      data != undefined ? data : !state.dialogComoFuncionaAberto;
  },
  alteraDialogMultiEtapasAberto(state, data) {
    state.dialogMultiEtapasAberto =
      data != undefined ? data : !state.dialogMultiEtapasAberto;
  },
  alteraDialogCapturarLeadAberto(state, data) {
    state.dialogCapturarLeadAberto =
      data != undefined ? data : !state.dialogCapturarLeadAberto;
  },
  alteraDialogArquivoHTML(state, data) {
    if (data == false) {
      state.dialogArquivoHTML = {
        aberto: false,
        titulo: "",
        link: ""
      };
    } else {
      state.dialogArquivoHTML = data;
    }
  },
  alteraDialogIframe(state, data) {
    if (data == false) {
      state.dialogIframe = {
        aberto: false,
        link: ""
      };
    } else {
      state.dialogIframe = data;
    }
  },
  alteraDialogCondicoesGeraisAberto(state, data) {
    state.dialogCondicoesGeraisAberto =
      data != undefined ? data : !state.dialogCondicoesGeraisAberto;
  }
};
