export const state = () => ({
  titulosSelecionados: [],
  tipoAtualTitulos: null,
  origemTitulos: null, //1 - escolha manual; 2 - escolha por quantidade; 3 - adição automática no checkout;
  meiosDePagamentos: null,
  transacaoPendente: null,
  etapasTransacaoPendente: null,
  checkoutPendente: null,
  ignoraCheckoutPendente: false,
  temPremioInstantaneo: true,
  codigoVendedor: null,
  compraComBoleto: null,
  afiliacao: "Store",
  tipoPagamentoInicial: 10
});

export const getters = {
  titulosSelecionados(state) {
    return state.titulosSelecionados;
  },
  totalizador(state) {
    const total = {
      qtd: state.titulosSelecionados.length,
      soma: state.titulosSelecionados.reduce(
        (total, item) => total + item.valor,
        0
      )
    };
    return total;
  },
  tipoAtualTitulos(state) {
    return state.tipoAtualTitulos;
  },
  meiosDePagamentos(state) {
    return state.meiosDePagamentos;
  },
  origemTitulos(state) {
    return state.origemTitulos;
  },
  transacaoPendente(state) {
    return state.transacaoPendente;
  },
  etapasTransacaoPendente(state) {
    return state.etapasTransacaoPendente;
  },
  checkoutPendente(state) {
    return state.checkoutPendente;
  },
  ignoraCheckoutPendente(state) {
    return state.ignoraCheckoutPendente;
  },
  codigoVendedor(state) {
    return state.codigoVendedor;
  },
  afiliacao(state) {
    return state.afiliacao;
  },
  tipoPagamentoInicial(state) {
    return state.tipoPagamentoInicial;
  },
  temPremioInstantaneo(state) {
    return state.temPremioInstantaneo;
  },
  compraComBoleto(state) {
    return state.compraComBoleto;
  }
};

export const mutations = {
  adicionaTitulo(state, data) {
    if (data.origem && state.origemTitulos != data.origem) {
      state.titulosSelecionados = [];
      state.tipoAtualTitulos = null;
    }
    if (
      state.titulosSelecionados.find(t => t.id == data.titulo.id) == undefined
    ) {
      state.titulosSelecionados = state.titulosSelecionados.concat([
        JSON.parse(JSON.stringify(data.titulo))
      ]);
      state.tipoAtualTitulos = data.tipo;
      if (data.origem) {
        state.origemTitulos = data.origem;
      }
      this.$gtagEvent("ADD_TO_CART", data.titulo);
      this.$fpEvent("ADD_TO_CART", data.titulo);
    }
  },
  removeTitulo(state, data) {
    let position;
    let card;
    state.titulosSelecionados = state.titulosSelecionados.filter((t, i) => {
      if (t.id == data) {
        position = i + 1;
        card = t;
      }
      return t.id != data;
    });
    this.$gtagEvent("REMOVE_FROM_CART", { position, card });
  },
  removerUltimoTitulo(state) {
    let position = state.titulosSelecionados.length;
    let card = state.titulosSelecionados[position - 1];
    this.$gtagEvent("REMOVE_FROM_CART", { position, card });
    state.titulosSelecionados.pop();
  },
  alterarOrigemTitulos(state, data) {
    state.origemTitulos = data;
  },
  removeTitulosOrigem3(state) {
    if (state.origemTitulos == 3) {
      state.titulosSelecionados.forEach((t, i) => {
        let position = i + 1;
        let card = t;
        this.$gtagEvent("REMOVE_FROM_CART", { position, card });
      });
      state.titulosSelecionados = [];
      state.tipoAtualTitulos = null;
      state.origemTitulos = null;
    }
  },
  salvaMeiosDePagamentos(state, data) {
    state.meiosDePagamentos = data;
  },
  limpaTitulosSelecionados(state, parcial = false) {
    state.titulosSelecionados.forEach((t, i) => {
      let position = i + 1;
      let card = t;
      this.$gtagEvent("REMOVE_FROM_CART", { position, card });
    });
    state.titulosSelecionados = [];
    if (!parcial) {
      state.tipoAtualTitulos = null;
      state.origemTitulos = null;
    }
  },
  salvaTransacaoPendente(state, data) {
    state.transacaoPendente = data;
  },
  salvaEtapasTransacaoPendente(state, data) {
    state.etapasTransacaoPendente = data;
  },
  salvaCheckoutPendente(state, data) {
    state.checkoutPendente = data;
  },
  ignorarCheckoutPendente(state) {
    state.ignoraCheckoutPendente = true;
  },
  salvaCodigoVendedor(state, data) {
    state.codigoVendedor = data;
  },
  salvaAfiliacao(state, data) {
    state.afiliacao = data;
  },
  salvaTipoPagamentoInicial(state, data) {
    state.tipoPagamentoInicial = data;
  },
  salvaTemPremioInstantaneo(state, data) {
    state.temPremioInstantaneo = data;
  },
  salvaCompraComBoleto(state, data) {
    state.compraComBoleto = data;
  }
};
