export const state = () => ({
  logado: false,
  dados: null,
  dadosComprarSemLogar: null,
  token: null,
  lembrarLogin: false,
  loginSalvo: null,
  historicoDeTitulos: null,
  contaCorrente: null,
  destinoLogin: "/comprar",
  origemTrafego: null,
  dadosCapturaLead: null,
  dadosVendedorLead: null,
  capturaLeadPersistente: null
});

export const getters = {
  logado(state) {
    return state.logado;
  },
  primeiroNome(state) {
    const pNome = state.dados ? state.dados.nome.split(" ")[0] : "";
    return pNome;
  },
  dados(state) {
    return state.dados;
  },
  token(state) {
    return state.token;
  },
  destinoLogin(state) {
    return state.destinoLogin;
  },
  historicoDeTitulos(state) {
    return state.historicoDeTitulos;
  },
  contaCorrente(state) {
    return state.contaCorrente;
  },
  origemTrafego(state) {
    return state.origemTrafego;
  },
  dadosComprarSemLogar(state) {
    return state.dadosComprarSemLogar;
  },
  dadosCapturaLead(state) {
    return state.dadosCapturaLead;
  },
  dadosVendedorLead(state) {
    return state.dadosVendedorLead;
  },
  capturaLeadPersistente(state) {
    return state.capturaLeadPersistente;
  }
};

export const mutations = {
  login(state, data) {
    state.logado = true;
    state.token = data.token;
    state.dados = data.perfil;
  },
  logout(state) {
    state.logado = false;
    state.token = null;
    state.dados = null;
  },
  lembrarLogin(state, data) {
    state.lembrarLogin = true;
    state.loginSalvo = data;
  },
  removerLembrarLogin(state) {
    state.lembrarLogin = false;
    state.loginSalvo = null;
  },
  salvaDestinoLogin(state, data) {
    state.destinoLogin = data;
  },
  salvaHistoricoDeTitulos(state, data) {
    state.historicoDeTitulos = data;
  },
  salvaDados(state, data) {
    state.dados = data;
  },
  salvaDadosComprarSemLogar(state, data) {
    state.dadosComprarSemLogar = data;
  },
  salvaContaCorrente(state, data) {
    state.contaCorrente = data;
  },
  salvaOrigemTrafego(state, data) {
    state.origemTrafego = data;
  },
  salvaDadosCapturaLead(state, data) {
    state.dadosCapturaLead = data;
  },
  salvaCapturaLeadPersistente(state, data) {
    state.capturaLeadPersistente = data;
  },
  salvaDadosVendedorLead(state, data) {
    state.dadosVendedorLead = data;
  }
};
