var VueScrollTo = require("vue-scrollto");
var start = false;
var options = {
  offset: -30,
  force: true,
  cancelable: false,
  lazy: false,
  onStart: () => {
    start = true;
  }
};
export default function(ctx) {
  if (ctx.route.query && ctx.route.query.area) {
    switch (ctx.route.query.area) {
      case "quero-concorrer":
        options.offset = -100;
        break;
      case "pagamento":
        options.offset = -5;
        break;

      default:
        break;
    }
    start = false;
    let tempo = 1000;
    do {
      setTimeout(() => {
        if (!start && ctx.store.getters["praca/carregou"]) {
          VueScrollTo.scrollTo(
            document.getElementById(ctx.route.query.area),
            1500,
            options
          );
        }
      }, tempo);
      tempo += 500;
    } while (!start && tempo <= 5000);
  }
}
