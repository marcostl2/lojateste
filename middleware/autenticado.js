export default function(ctx) {
  if (!ctx.store.getters["usuario/logado"]) {
    if (ctx.route.path == "/comprar") {
      ctx.redirect(`/comprar/cadastrounico`);
    } else {
      ctx.store.commit("usuario/salvaDestinoLogin", ctx.route.fullPath);
      ctx.redirect(`/entrar`);
    }
  }
}
