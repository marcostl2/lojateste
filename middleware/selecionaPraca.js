import axios from "axios";

export default function(ctx) {
  scripts(ctx);
}
async function scripts(ctx) {
  const pracaCarregada = ctx.store.getters["praca/carregada"];
  let configPraca;
  let configPadrao;
  let nomePraca;
  let caminhoURL = ctx.route.fullPath;
  let rotaDestino;
  let nomePasta = ctx.store.getters["praca/nomePasta"];
  if (!pracaCarregada && caminhoURL.indexOf(`/error`) != 0) {
    configPadrao = await axios.get("/config.json").then(res => res.data);
    if (configPadrao.pracaUnica && configPadrao.pracaUnica.length > 0) {
      configPraca = await buscaPraca(configPadrao.pracaUnica);
      nomePraca = configPadrao.pracaUnica;
      ctx.store.commit("praca/salvaTipoUnica", true);
      await salvaConfigs();
    } else {
      ctx.store.commit("praca/salvaTipoUnica", false);
      nomePraca = caminhoURL.split("/")[1];
      nomePraca =
        nomePraca.indexOf("?") != -1 ? nomePraca.split("?")[0] : nomePraca;
      configPraca =
        nomePraca.length > 0 && nomePraca.charAt(0) != "#"
          ? await buscaPraca(nomePraca)
          : null;
      if (isJson(configPraca)) {
        nomePasta = null;
        await salvaConfigs();
        rotaDestino = caminhoURL.replace(`/${nomePraca}`, "");
        rotaDestino =
          rotaDestino.indexOf("/") == 0 ? rotaDestino : `/${rotaDestino}`;
        ctx.redirect(rotaDestino);
      } else {
        if (nomePasta) {
          configPraca = await buscaPraca(nomePasta);
          await salvaConfigs();
          const link = ctx;
        } else {
          ctx.redirect("/error");
        }
      }
    }
  } else {
    const etapaAtual = ctx.store.getters["praca/etapaAtual"];
    if (!etapaAtual) {
      redirecionaSemEtapa();
    }
  }
  async function salvaConfigs() {
    ctx.store.commit("praca/salvaConfig", configPraca);
    if (!nomePasta) {
      ctx.store.commit("praca/salvaNomePasta", nomePraca);
    }
    await carregarAparencia();
    await carregarEtapaAtual();
    validaEtapaAtual();
  }
  async function buscaPraca(obj) {
    let data;
    let resPracaAPI;
    try {
      resPracaAPI = await ctx.app.$networkApi.informacaoPraca(
        obj.toLowerCase()
      );
      if (resPracaAPI && resPracaAPI.data) {
        const dataPracaAPI = resPracaAPI.data;
        const resLojaOnline = await ctx.app.$firebaseApi.getLojaOnline(
          dataPracaAPI.codigo_praca
        );
        if (resLojaOnline && resLojaOnline.data) {
          const dataLojaOnline = resLojaOnline.data;
          data = { dataPracaAPI, dataLojaOnline };
        }
      }
    } catch (error) {}
    return data;
  }

  async function carregarAparencia() {
    const config = ctx.store.getters["praca/config"];
    try {
      const res = await ctx.$firebaseApi.getAparencia(config.codigoPraca);
      if (res && res.data) {
        ctx.store.commit("praca/salvaAparencia", res.data);
      }
    } catch (error) {}
  }
  async function carregarEtapaAtual() {
    try {
      const res = await ctx.$networkApi.carregaEtapas();
      ctx.store.commit("praca/salvaEtapa", res.data);
    } catch (error) {}
  }
  function validaEtapaAtual() {
    if (ctx.route.query && ctx.route.query.e) {
      const id = parseInt(ctx.route.query.e.replace(/[^0-9]/g, ""));
      if (!isNaN(id)) {
        ctx.store.commit("praca/salvaIdEtapaAtual", id);
      }
    }
    const etapaAtual = ctx.store.getters["praca/etapaAtual"];
    if (!etapaAtual) {
      redirecionaSemEtapa();
    } else {
      ctx.store.commit("praca/salvaIdEtapaAtual", etapaAtual.id);
    }
  }
  function redirecionaSemEtapa() {
    const rotaAtual = ctx.route.path.toLowerCase();
    const rotasNaoPermitidas = [
      "/",
      "/comprar",
      "/comprar/processapagamento",
      "/comprar/sucesso",
      "/comprartitulos"
    ];
    if (rotasNaoPermitidas.indexOf(rotaAtual) > -1) {
      ctx.redirect(`/semEtapa`);
    }
  }
  function isJson(str) {
    try {
      if (typeof str === "object" && str !== null) {
        return true;
      } else if (str == null) {
        return false;
      }
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
}
