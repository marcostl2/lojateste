export default function(ctx) {
  if (
    ctx.route.query.origem &&
    ctx.route.query.origem.toLowerCase() == "app-sem-login"
  ) {
    const afiliacao = ctx.store.getters["compra/afiliacao"];
    if (afiliacao != "app-sem-login") {
      ctx.store.commit("compra/salvaAfiliacao", "app-sem-login");
      ctx.app.$gtagEvent("APP_SEM_LOGIN");
    }
  }
}
