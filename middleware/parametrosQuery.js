export default function(ctx) {
  if (ctx.route.query) {
    if (ctx.route.query.ven) {
      ctx.store.commit(
        "compra/salvaCodigoVendedor",
        ctx.route.query.ven.toLowerCase()
      );
    }
    if (ctx.route.query.pgto) {
      switch (ctx.route.query.pgto.toLowerCase()) {
        case "boleto":
          ctx.store.commit("compra/salvaTipoPagamentoInicial", 11);
          break;

        default:
          break;
      }
    }
    if (
      (ctx.route.query.pdv_code && ctx.route.query.pdv_source) ||
      ctx.route.query.c
    ) {
      if (ctx.store.getters["usuario/logado"]) {
        const dados = ctx.store.getters["usuario/dados"];
        const cpf = dados.cpf.replace(/[^0-9]/g, "");
        const email = dados.email;
        ctx.store.commit("usuario/salvaDadosCapturaLead", { cpf, email });
      } else {
        ctx.store.commit("layout/alteraDialogCapturarLeadAberto", true);
      }
    }
  }
}
