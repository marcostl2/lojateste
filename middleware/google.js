export default function(ctx) {
  const origemTrafego = ctx.store.getters["usuario/origemTrafego"];
  if (ctx.route.path.toLowerCase().indexOf("processapagamento") > -1) {
    delete window.document.referrer;
    window.document.__defineGetter__("referrer", () => {
      return origemTrafego ? origemTrafego : "";
    });
  }

  registra();

  function registra() {
    setTimeout(() => {
      const carregou = ctx.store.getters["praca/carregou"];
      const params = {
        page_title: document.title,
        page_path: ctx.route.path,
        page_location: window.location.href
      };
      if (carregou) {
        ctx.app.$gaPageView(params);
      } else {
        registra();
      }
    }, 1000);
  }
}
