export default function(ctx) {
  const carregou = ctx.store.getters["praca/carregou"];
  setTimeout(() => {
    if (carregou) {
      ctx.app.$fpPageView();
    }
  }, 1000);
}
