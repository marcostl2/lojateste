export default function(ctx) {
  if (ctx.store.getters["usuario/logado"]) {
    ctx.redirect(`/`);
  }
}
