export default function(ctx) {
  switch (ctx.route.path.toLowerCase()) {
    case "/payment/":
    case "/payment":
      ctx.redirect(`/pagamento`);
      break;
  }
}
