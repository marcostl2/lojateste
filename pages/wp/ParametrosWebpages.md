# Indique e ganhe

Rota: {URL}/wp/indique?p={id_da_praca}&id={id_do_cliente}

# Meu Time

Rota: {URL}/wp/meuTime?p={id_da_praca}&c={codigo_distribuidor}

# Aceite/optout

Rota: {URL}/wp/aceite?p={id_da_praca}&c={nsu}&url={url}

# Produtos adquiridos

Rota:

{URL}/wp/produtosAdquiridos?p={id_da_praca}&praca={codigo_da_praca}
ou
{URL}/wp/produtosAdquiridos?p={id_da_praca}&uuid={uuid_da_venda}
